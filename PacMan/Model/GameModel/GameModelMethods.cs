﻿using Core;
using PacMan.Model.Images;
using Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PacMan.Model.GameModel
{
    public partial class GameModel
    {
        public void StartGame()
        {
            _logger.Info("Start Game");
            if (!IsGameNotBegun)
                return;
            IsGameNotBegun = false;

            StartTimers();
        }

        public void ReCreateGame()
        {
            _logger.Info("Re Create Game");

            StopTimers();

            IsGameOver = false;

            _score = 0;
            _lives = 3;

            GhostsMovingAlgorithms = new List<IMovingAlgorithm>();
            _fires = new List<Fire>();

            void Action()
            {
                ReCreateLevel();
            }
            CallApplicationDispatcherBeginInvoke(Action);
        }

        public void ChangeAlgorithm(IMovingAlgorithm algorithm)
        {
            _logger.Info("Changed the algorithm for the movement of ghosts to " + algorithm.Name);

            SelectedAlgorithm = algorithm;

            foreach (var ghost in _ghosts)
            {
                ghost.ChangeAlgorithm(SelectedAlgorithm);
            }
        }
        
        public void ChangeName(string newName)
        {
            _logger.Info($"Changed username from {PlayerName} to {newName}");

            PlayerName = newName;
        }

        public void GetMovingAlgorithms()
        {
            GhostsMovingAlgorithms.Clear();

            System.IO.Directory.CreateDirectory(StaticStrings.FolderPlugins);
            var catalog = new DirectoryCatalog(StaticStrings.FolderPlugins);
            var container = new CompositionContainer(catalog);
            var importer = new Importer();

            container.ComposeExportedValue(MapMatrix);
            container.ComposeParts(importer);

            foreach (var import in importer.ImportedMembers)
            {
                GhostsMovingAlgorithms.Add(import);
            }

            _selectedAlgorithm = new GhostMovingBasicAlgorithm(MapMatrix);
            GhostsMovingAlgorithms.Add(_selectedAlgorithm);
        }

        public void SetPucManDirection(Direction direction)
        {
            _logger.Info($"Changed direction PucMan from {_selectPucManDirection} to {direction}");
            _selectPucManDirection = direction;
        }

        private void GameOver()
        {
            try
            {
                _logger.Info($"Game Over. Score: {Score}");
                _dataBase.Records.Create(CurrentRecord);
                _dataBase.Save();
                Score = 0;
            }
            catch (Exception ex)
            {
                _logger.Error("Error in save record in db " + ex.Message);
                // ignored
            }
        }

        private void ReCreateLevel()
        {
            _logger.Info("Re Create Level");

            IsGameNotBegun = true;

            _ghosts = new List<Ghost>();
            CreateLocation();
            GetMovingAlgorithms();
            CreateWall();
            CreatePacMan();
            CreateGhosts();
            CreateFires();
            ToStartPosition();
            TimeCurrentGame = new TimeSpan(0, 3, 0);
        }

        private void CreateLocation()
        {
            MapMatrix = _matrixCreator.Generate();

            _logger.Info("Create new map matrix ");
            PrintMapMatrixToLog();

            _logger.Info($"creating canvas proportional to the matrix: Width = {SizeObjectsOnMap * NumberHorizontalBlocksMap}; Height = {SizeObjectsOnMap * NumberVerticalBlocksMap}");
            Location = new Canvas { Background = new SolidColorBrush(Colors.Black), Width = SizeObjectsOnMap * NumberHorizontalBlocksMap, Height = SizeObjectsOnMap * NumberVerticalBlocksMap };
        }

        private void CreatePacMan()
        {
            var pacManMovingControl = new MapMovingControl(MapMatrix, NumberVerticalBlocksMap * SizeObjectsOnMap, NumberHorizontalBlocksMap * SizeObjectsOnMap, TimeUpdatePucMan,
                StaticStrings.PacManUpImg, StaticStrings.PacManLeftImg, StaticStrings.PacManRightImg, StaticStrings.PacManDownImg, StaticStrings.PacManBasicImg);

            _currentPucMan = new PucMan(pacManMovingControl, StepMovingObjects, StaticStrings.PacManBasicImg, _pacManStartPositionPoint);

            _logger.Info($"Pacman placement on canvas X={_pacManStartPositionPoint.X}; Y={_pacManStartPositionPoint.Y}");
            Location.Children.Add(_currentPucMan.Image);
        }

        private void CreateWall()
        {
            var wallIm = new BitmapImage();
            wallIm.BeginInit();
            wallIm.UriSource = new Uri(StaticStrings.WallImg, UriKind.Relative);
            wallIm.EndInit();

            for (var row = 0; row < NumberVerticalBlocksMap; row++)
            {
                for (var col = 0; col < NumberHorizontalBlocksMap; col++)
                {
                    if (MapMatrix[row,col])
                        continue;

                    var wall = new Image()
                    {
                        Width = SizeObjectsOnMap,
                        Height = SizeObjectsOnMap,
                        Source = wallIm
                    };

                    Canvas.SetTop(wall, row * SizeObjectsOnMap);
                    Canvas.SetLeft(wall, col * SizeObjectsOnMap);
                    Location.Children.Add(wall);
                }
            }
        }
        
        private void CreateGhosts()
        {
            var ghostPinkMovingControl = new MapMovingControl(MapMatrix, NumberVerticalBlocksMap * SizeObjectsOnMap, NumberHorizontalBlocksMap * SizeObjectsOnMap, TimeUpdateGhosts,
                StaticStrings.PinkGhostUpImg, StaticStrings.PinkGhostLeftImg, StaticStrings.PinkGhostRightImg, StaticStrings.PinkGhostDownImg, StaticStrings.PinkGhostBasicImg, false);
            var ghostRedMovingControl = new MapMovingControl(MapMatrix, NumberVerticalBlocksMap * SizeObjectsOnMap, NumberHorizontalBlocksMap * SizeObjectsOnMap, TimeUpdateGhosts,
                StaticStrings.RedGhostUpImg, StaticStrings.RedGhostLeftImg, StaticStrings.RedGhostRightImg, StaticStrings.RedGhostDownImg, StaticStrings.RedGhostBasicImg, false);
            var ghostBlueMovingControl = new MapMovingControl(MapMatrix, NumberVerticalBlocksMap * SizeObjectsOnMap, NumberHorizontalBlocksMap * SizeObjectsOnMap, TimeUpdateGhosts,
                StaticStrings.BlueGhostUpImg, StaticStrings.BlueGhostLeftImg, StaticStrings.BlueGhostRightImg, StaticStrings.BlueGhostDownImg, StaticStrings.BlueGhostBasicImg, false);
            var ghostYellowMovingControl = new MapMovingControl(MapMatrix, NumberVerticalBlocksMap * SizeObjectsOnMap, NumberHorizontalBlocksMap * SizeObjectsOnMap, TimeUpdateGhosts,
                StaticStrings.YellowGhostUpImg, StaticStrings.YellowGhostLeftImg, StaticStrings.YellowGhostRightImg, StaticStrings.YellowGhostDownImg, StaticStrings.YellowGhostBasicImg, false);

            var pinkGhostPosition = CreateGhostMapPoint(_pseudoRandom);
            var pinkGhost = new Ghost(ghostPinkMovingControl, SelectedAlgorithm, StepMovingObjects, StaticStrings.PinkGhostBasicImg,
                pinkGhostPosition, SizeObjectsOnMap * NumberHorizontalBlocksMap, SizeObjectsOnMap * NumberVerticalBlocksMap);
            _ghosts.Add(pinkGhost);
            _logger.Info($"pinkGhost placement on canvas X={pinkGhostPosition.X}; Y={pinkGhostPosition.Y}");
            Location.Children.Add(pinkGhost.Image);

            var redGhostPosition = CreateGhostMapPoint(_pseudoRandom);
            var redGhost = new Ghost(ghostRedMovingControl, SelectedAlgorithm, StepMovingObjects, StaticStrings.RedGhostBasicImg,
                redGhostPosition, SizeObjectsOnMap * NumberHorizontalBlocksMap, SizeObjectsOnMap * NumberVerticalBlocksMap,
                Direction.Down);
            _ghosts.Add(redGhost);
            _logger.Info($"redGhost placement on canvas X={redGhostPosition.X}; Y={redGhostPosition.Y}");
            Location.Children.Add(redGhost.Image);

            var blueGhostPosition = CreateGhostMapPoint(_pseudoRandom);
            var blueGhost = new Ghost(ghostBlueMovingControl, SelectedAlgorithm, StepMovingObjects, StaticStrings.BlueGhostBasicImg,
                blueGhostPosition, SizeObjectsOnMap * NumberHorizontalBlocksMap, SizeObjectsOnMap * NumberVerticalBlocksMap,
                Direction.Left);
            _ghosts.Add(blueGhost);
            _logger.Info($"blueGhost placement on canvas X={blueGhostPosition.X}; Y={blueGhostPosition.Y}");
            Location.Children.Add(blueGhost.Image);

            var yellowGhostPosition = CreateGhostMapPoint(_pseudoRandom);
            var yellowGhost = new Ghost(ghostYellowMovingControl, SelectedAlgorithm, StepMovingObjects, StaticStrings.YellowGhostBasicImg,
                yellowGhostPosition, SizeObjectsOnMap * NumberHorizontalBlocksMap, SizeObjectsOnMap * NumberVerticalBlocksMap,
                Direction.Right);
            _ghosts.Add(yellowGhost);
            _logger.Info($"yellowGhost placement on canvas X={yellowGhostPosition.X}; Y={yellowGhostPosition.Y}");
            Location.Children.Add(yellowGhost.Image);
        }

        private CanvasObject CreateGhostMapPoint(Random pseudoRandom)
        {
            var spot = new CanvasObject();

            while (true)
            {
                var xMatrixCoordinate = pseudoRandom.Next(5, NumberHorizontalBlocksMap - 1);
                var yMatrixCoordinate = pseudoRandom.Next(5, NumberVerticalBlocksMap - 1);

                if (!MapMatrix[yMatrixCoordinate,xMatrixCoordinate]) continue;
                spot.X = xMatrixCoordinate * SizeObjectsOnMap;
                spot.Y = yMatrixCoordinate * SizeObjectsOnMap;
                break;
            }
            spot.Width = SizeObjectsOnMap;
            spot.Height = SizeObjectsOnMap;

            return spot;
        }

        private bool IsWhetherCaught()
        {
            foreach (var ghost in _ghosts)
            {
                var distance = Math.Sqrt(Math.Pow(Math.Abs(ghost.CanvasCoordinateX - _currentPucMan.CanvasCoordinateX), 2) +
                                            Math.Pow(Math.Abs(ghost.CanvasCoordinateY - _currentPucMan.CanvasCoordinateY), 2));

                if (distance <= SizeObjectsOnMap-StepMovingObjects)
                {
                    _logger.Info("Ghost killed PacMan");
                    return true;
                }
            }
            return false;
        }

        private bool IsTimeOut()
        {
            bool timeOut = _timeCurrentGameCurrentGame < _timeGameOver;
            if (timeOut)
            {
                _logger.Info("Time Out");
                return true;
            }
            return false;
        }

        private void CreateFires()
        {
            for (int i = 0; i < CountFires; i++)
            {
                var fire = new Fire(SizeObjectsOnMap, MapMatrix, _fires, _pseudoRandom);
                _fires.Add(fire);
                Location.Children.Add(fire.FireImage);
            }
        }

        private void PacManSeekingFire()
        {
            var fire = FindEatenFire();
            if (fire == null) return;

            void Action()
            {
                Location.Children.Remove(fire.FireImage);
            }
            CallApplicationDispatcherBeginInvoke(Action);
            _logger.Info("PacMan eats fire. Score +100");
            _fires.Remove(fire);
            Score = _score + 100;
        }

        private void LevelVin()
        {
            IsGameNotBegun = true;
            StopTimers();
            int bonus = (TimeCurrentGame.Minutes * 60) + TimeCurrentGame.Seconds;
            
            _logger.Info($"Level Vin. Score +{bonus}");
            Score = Score + bonus;
            void Action()
            {
                ReCreateLevel();
            }
            CallApplicationDispatcherBeginInvoke(Action);
        }

        private Fire FindEatenFire()
        {
            foreach (var fire in _fires)
            {
                var distance = Math.Sqrt(Math.Pow(Math.Abs(fire.CanvasCoordinateX - _currentPucMan.CanvasCoordinateX), 2) +
                                         Math.Pow(Math.Abs(fire.CanvasCoordinateY - _currentPucMan.CanvasCoordinateY), 2));

                if (distance <= StepMovingObjects)
                {
                    return fire;
                }
            }
            return null;
        }

        private void CallApplicationDispatcherBeginInvoke(Action action)
        {
            try
            {
                Application.Current.Dispatcher.BeginInvoke(action);
            }
            catch (Exception)
            {
                //_logger.Error("Error in Begin Invoke " + ex.Message);

                StopTimers();
            }
        }

        private bool CheckIsGameOver()
        {
            if (Lives > 0)
            {
                Lives--;
                if (Lives==0)
                {
                    return IsGameOver = true;
                }
                void Action()
                {
                    ToStartPosition();
                }
                CallApplicationDispatcherBeginInvoke(Action);

                return IsGameOver = false;
            }
            return IsGameOver = true;
        }

        private void ToStartPosition()
        {
            IsGameNotBegun = true;

            var co = new CanvasObject() { Height = SizeObjectsOnMap, Width = SizeObjectsOnMap, X = 0, Y = 0 };

            _currentPucMan.ToPosition(co);

            foreach (var ghost in _ghosts)
            {
                ghost.ToPosition(CreateGhostMapPoint(_pseudoRandom));
            }
        }

        private void PrintMapMatrixToLog()
        {
            _logger.Info($"Map matrix contains {NumberVerticalBlocksMap} rows by {NumberHorizontalBlocksMap} cells");

            for (var y = 0; y < NumberVerticalBlocksMap; y++)
            {
                var sRow = new StringBuilder();

                for (var x = 0; x < NumberHorizontalBlocksMap; x++)
                {
                    sRow.Append(MapMatrix[y, x] ? $"Cell {x + 1}: passage; " :
                                                  $"Cell {x + 1}: wall;    ");
                }
                _logger.Info($"Map matrix {y+1} Row: {sRow}");
            }
            
        }
    }
}
