﻿using Core;
using Services;
using System;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace PacMan.Model
{
    public class MovingGameObject : ICanvasObject
    {
        protected readonly int MovementStep;

        public CanvasObject CanvasPoint { get; private set; }

        protected readonly MapMovingControl MovingControl;

        public int CanvasCoordinateX => CanvasPoint.X;

        public int CanvasCoordinateY => CanvasPoint.Y;

        public int CanvasWidth => CanvasPoint.Width;

        public int CanvasHeight => CanvasPoint.Height;

        public Image Image { get; private set; }

        public MovingGameObject(MapMovingControl movingControl, int speed, CanvasObject mapPoint)
        {
            MovementStep = speed;
            CanvasPoint = mapPoint;
            MovingControl = movingControl;
        }

        protected void InitImage(string basicIm)
        {
            var defaultIm = new BitmapImage();
            defaultIm.BeginInit();
            defaultIm.UriSource = new Uri(basicIm, UriKind.Relative);
            defaultIm.EndInit();
            Image = new Image { Width = CanvasWidth, Height = CanvasHeight, Source = defaultIm };
        }

        public void Move(Direction direction)
        {
            MovingControl.MoveMapObject(Image, direction, CanvasPoint, MovementStep);
        }

        public void ToPosition(CanvasObject mapPoint)
        {
            MovingControl.UpdatePosition(Image, CanvasPoint, mapPoint);
        }

    }
}
