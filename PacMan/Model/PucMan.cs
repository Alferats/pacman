﻿using Core;
using NLog;
using Services;
using System.Windows.Controls;

namespace PacMan.Model
{
    public class PucMan : MovingGameObject
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public PucMan(MapMovingControl movingControl, int speed, string basicIm, CanvasObject mapPoint)
            :base(movingControl, speed, mapPoint)
        {
            InitImage(basicIm);

            Canvas.SetTop(Image, CanvasCoordinateY);
            Canvas.SetLeft(Image, CanvasCoordinateX);
        }

        public new void Move(Direction direction)
        {
            _logger.Info($"PacMan try to move from: X={CanvasPoint.X}; Y={CanvasPoint.Y}; in the direction: {direction}");
            MovingControl.MoveMapObject(Image, direction, CanvasPoint, MovementStep);
            _logger.Info($"PacMan position: X={CanvasPoint.X}; Y={CanvasPoint.Y};");
        }


    }
}
