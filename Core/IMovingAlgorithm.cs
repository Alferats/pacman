﻿namespace Core
{
    public interface IMovingAlgorithm
    {
        string Name { get; set; }

        bool[,] MapMatrix { get;}

        Direction FindDirection(CanvasObject pucMan, CanvasObject pursuer);

    }
}
